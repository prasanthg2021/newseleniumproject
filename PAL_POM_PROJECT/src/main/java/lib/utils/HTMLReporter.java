package lib.utils;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public abstract class HTMLReporter {

	public static ExtentHtmlReporter html;
	public static ExtentReports extent;
	public ExtentTest testSuite, test;
	public String testCaseName, testDescription, nodes, author, category;
	
	public void startReport() {
			html = new ExtentHtmlReporter("./src/main/java/reports/SuperAdmin_reports.html");
			html.setAppendExisting(true);
			html.loadXMLConfig("./src/main/java/propertiesFile/extent-config.xml");
			extent = new ExtentReports();
			extent.attachReporter(html); 
	}

	public ExtentTest startTestCase(String testCaseName, String testDescription) {
		testSuite = extent.createTest(testCaseName, testDescription);		
		return testSuite;
	}

	public ExtentTest startTestModule(String nodes) {
		test = testSuite.createNode(nodes);
		return test;
	}

	public abstract long takeSnap();
	protected long snapNumber = 100000L;
	public void reportStep(String desc, String status, boolean bSnap)  {

		MediaEntityModelProvider img = null;
		if(bSnap && !status.equalsIgnoreCase("INFO")){
			snapNumber = takeSnap();   
			try {
				img = MediaEntityBuilder.createScreenCaptureFromPath
						("..\\reports\\images\\"+snapNumber+".jpg").build();
				} catch (IOException e) {	
			}
		}
		if(status.equalsIgnoreCase("PASS")) {
			test.pass(desc, img);	
		}else if (status.equalsIgnoreCase("FAIL")) {
			test.fail(desc, img);
			throw new RuntimeException();
		}else if (status.equalsIgnoreCase("WARNING")) {
			test.warning(desc, img);
		}else if (status.equalsIgnoreCase("INFO")) {
			test.info(desc);
		}						
	}
	
	public void reportStep(String desc, String status) {
		reportStep(desc, status, true);
	}

	public void endResult() {
		extent.flush();
	}		


}
