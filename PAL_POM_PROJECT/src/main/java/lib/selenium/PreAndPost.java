package lib.selenium;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import lib.utils.DataInputProvider;

public class PreAndPost extends WebDriverServiceImpl{

	public String dataSheetName, sheetName;	
    
	@BeforeSuite
	public void beforeSuite() {
		startReport();
	}
    
	@BeforeClass
	public void beforeClass() {
		startTestCase(testCaseName, testDescription);	
		System.out.println("Before class"); 
	}

	@BeforeMethod
	public void beforeMethod() throws EncryptedDocumentException, InvalidFormatException, IOException {
		//for reports		
		startTestModule(nodes);
		test.assignAuthor(author);
		test.assignCategory(category);
		startApp("chrome");
		//backupDataSheet();
	}

	public static void backupDataSheet() throws EncryptedDocumentException, InvalidFormatException, IOException {
		Workbook wb1 = null;
		SimpleDateFormat dt = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
		Date date = new Date();
		String date_s = dt.format(date);
		String path = System.getProperty("user.dir");
		String fileName = path + "\\data\\DataSheet.xlsx";
		FileInputStream fileInputStream = new FileInputStream(fileName);
		String fileExtensionName = fileName.substring(fileName.indexOf("."));
		if (fileExtensionName.equals(".xlsx")) {
			wb1 = new XSSFWorkbook(fileInputStream);
			XSSFFormulaEvaluator.evaluateAllFormulaCells(wb1);						
		}
		else if (fileExtensionName.equals(".xls")) {
			wb1 = new HSSFWorkbook(fileInputStream);
			HSSFFormulaEvaluator.evaluateAllFormulaCells(wb1);			
		}			                                                                                                                                                                                                                                                                               
		FileOutputStream fileOut = new FileOutputStream(path + "//data//DataSheetBackup//DataSheet_"+date_s+".xlsx");
		wb1.write(fileOut);
		fileOut.close();
	}

	@AfterMethod
	public void afterMethod() {
		closeAllBrowsers();
	}

	@AfterClass
	public void afterClass() {

	}

	@AfterTest
	public void afterTest() {

	}

	@AfterSuite
	public void afterSuite() throws Throwable {
		endResult();
	}

	@DataProvider(name="fetchData") 
	public  Object[][] getData(){		
		return DataInputProvider.getSheet(dataSheetName, sheetName);		
	}

}


