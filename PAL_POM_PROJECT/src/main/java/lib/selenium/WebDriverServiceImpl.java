package lib.selenium;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import lib.utils.HTMLReporter;


public class WebDriverServiceImpl extends HTMLReporter implements WebDriverService{

	public RemoteWebDriver driver;
	public ChromeOptions options;
	public Properties prop;

	public WebDriverServiceImpl() {
		prop = new Properties();
		try {
			prop.load(new FileInputStream(new File(System.getProperty("user.dir")+"./src/main/java/propertiesFile/locators.properties")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void startApp(String browser) {
		try {
			if(browser.equalsIgnoreCase("chrome")){
				System.setProperty("webdriver.chrome.driver", "./src/main/java/drivers/chromedriver.exe");
				driver = new ChromeDriver();  
			}else {
				System.setProperty("webdriver.gecko.driver", "./src/main/java/drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			reportStep("The browser: "+browser+" launched successfully", "PASS");
		} catch (WebDriverException e) {			
			reportStep("The browser: "+browser+" could not be launched", "FAIL");
		}
	}
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")){
				System.setProperty("webdriver.chrome.driver", "./src/main/java/drivers/chromedriver.exe");
				driver = new ChromeDriver(options); 
			}else {
				System.setProperty("webdriver.gecko.driver", "./src/main/java/drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.manage().window().maximize();
			driver.get(prop.getProperty(url));
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			reportStep("The browser: "+browser+" launched successfully", "PASS");
		} catch (WebDriverException e) {			
			reportStep("The browser: "+browser+" could not be launched", "FAIL");
		}
	}
	public void downloadFile() {
		String fileDownloadPath ;
		fileDownloadPath = System.getProperty("user.dir")+"\\src\\main\\java\\downloadFiles";
		Map<String, Object> prefsMap = new HashMap<String, Object>();
		prefsMap.put("profile.default_content_settings.popups", 0);
		prefsMap.put("download.default_directory", fileDownloadPath);

		options = new ChromeOptions();
		options.setExperimentalOption("prefs", prefsMap);  
		options.addArguments("--test-type");
		options.addArguments("--disable-extensions");
		options.addArguments("--incognito"); 
	}
	public void loadUrl(String url) {
		try {
			driver.get(prop.getProperty(url)); 
			reportStep("The url: "+url+" launched successfully", "PASS");
		} catch (Exception e) {
			reportStep("The url: "+url+" could not be launched", "FAIL");
		}
	}
	public WebElement locateElement(String locator, String locValue) {

		try {
			switch (locator) {
			case "id": return driver.findElementById(prop.getProperty(locValue));
			case "name": return driver.findElementByName(prop.getProperty(locValue));
			case "class": return driver.findElementByClassName(prop.getProperty(locValue));
			case "link" : return driver.findElementByLinkText(prop.getProperty(locValue));
			case "xpath": return driver.findElementByXPath(prop.getProperty(locValue));
			case "tagname": return driver.findElementByTagName(prop.getProperty(locValue));
			default:
				break;
			}
		} catch (NoSuchElementException e) {
			reportStep("The element with locator "+locator+" and locator value : "+prop.getProperty(locValue)+" not found.","FAIL");
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while finding "+locator+" and locator value : "+prop.getProperty(locValue)+" with the value "+locValue, "FAIL");
		}
		return null;
	}

	public WebElement locateElementWithoutProp(String locator, String locValue) {
		try {
			switch (locator) {
			case "id": return driver.findElementById(locValue);
			case "name": return driver.findElementByName(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "link" : return driver.findElementByLinkText(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			case "tagname": return driver.findElementByTagName(locValue);
			default:
				break;
			}
		} catch (NoSuchElementException e) {
			reportStep("The element with locator "+locator+" and locator value : "+prop.getProperty(locValue)+" not found.","FAIL");
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while finding "+locator+" and locator value : "+prop.getProperty(locValue)+" with the value "+locValue, "FAIL");
		}
		return null;
	}
	public WebElement locateElementWithXpath(String locValue) {
		WebElement elemetXPath = null; 
		try {
			elemetXPath = driver.findElementByXPath(prop.getProperty(locValue)); 
		} catch (NoSuchElementException e) {
			reportStep("The element with locator "+locValue+" not found.","FAIL");
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while finding "+locValue+" with the value "+locValue, "FAIL");
		}
		return elemetXPath;
	}
	public List<WebElement> locateElements(String locator, String locValue) {
		try {
			switch (locator) {
			case "id": return driver.findElementsById(prop.getProperty(locValue));
			case "name": return driver.findElementsByName(prop.getProperty(locValue));
			case "class": return driver.findElementsByClassName(prop.getProperty(locValue));
			case "link" : return driver.findElementsByLinkText(prop.getProperty(locValue));
			case "xpath": return driver.findElementsByXPath(prop.getProperty(locValue));
			case "tagname": return driver.findElementsByTagName(prop.getProperty(locValue));
			default:
				break;
			}
		} catch (NoSuchElementException e) {
			reportStep("The element with locator "+locator+" not found.","FAIL");
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while finding "+locator+" with the value "+locValue, "FAIL");
		}
		return null;
	}
	public WebElement locateElement(String locValue) {
		return driver.findElementById(prop.getProperty(locValue));
	}
	public void type(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
			reportStep("The data: "+data+" entered successfully in the field :"+ele, "PASS");
		} catch (InvalidElementStateException e) {
			reportStep("The data: "+data+" could not be entered in the field :"+ele,"FAIL");
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while entering "+data+" in the field :"+ele, "FAIL");
		}
	}
	public void typeValue(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data,Keys.ENTER);
			reportStep("The data: "+data+" entered successfully in the field :"+ele, "PASS");
		} catch (InvalidElementStateException e) {
			reportStep("The data: "+data+" could not be entered in the field :"+ele,"FAIL");
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while entering "+data+" in the field :"+ele, "FAIL");
		}
	}

	public void typeValueWithTab(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data,Keys.TAB);
			reportStep("The data: "+data+" entered successfully in the field :"+ele, "PASS");
		} catch (InvalidElementStateException e) {
			reportStep("The data: "+data+" could not be entered in the field :"+ele,"FAIL");
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while entering "+data+" in the field :"+ele, "FAIL");
		}
	}

	public void click(WebElement ele) {
		String text = "";
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			text = ele.getText();
			ele.click();
			reportStep("The element "+text+" is clicked", "PASS");
		} catch (InvalidElementStateException e) {
			reportStep("The element: "+text+" could not be clicked", "FAIL");
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL");
		} 
	}
	public void moveToElment(WebElement ele) {
		String text = "";
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));	
			Actions builder = new Actions(driver);
			builder.moveToElement(ele).pause(2000).perform(); 
			text = ele.getText();
			builder.click(ele).perform(); 
			reportStep("The element "+text+" is clicked", "PASS");
		} catch (InvalidElementStateException e) {
			reportStep("The element: "+text+" could not be clicked", "FAIL");
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while clicking in the field : "+text, "FAIL");
		} 
	}
	public void jsClick(WebElement ele) {
		String text = "";
		try {
			text = ele.getText();
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", ele);
			reportStep("The element "+text+" is clicked", "PASS");
		} catch (InvalidElementStateException e) {
			reportStep("The element: "+text+" could not be clicked", "FAIL");
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL");
		} 
	}
	public void clickWithNoSnap(WebElement ele) {
		String text = "";
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));	
			text = ele.getText();
			ele.click();			
			reportStep("The element :"+text+"  is clicked.", "PASS",false);
		} catch (InvalidElementStateException e) {
			reportStep("The element: "+text+" could not be clicked", "FAIL",false);
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while clicking in the field :","FAIL",false);
		} 
	}

	public void dragAndDrop(WebElement ele1, WebElement ele2) {
		try {
			Actions builder = new Actions(driver);
			builder.dragAndDrop(ele1, ele2).perform();
			reportStep("The element dropped successfully", "PASS");
		} catch (InvalidElementStateException e) {
			reportStep("The element not dropped successfully","FAIL");
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while dragging the elemnt", "FAIL");
		}
	}
	public void clickAndHold(WebElement ele1, WebElement ele2) {
		try {
			Actions builder = new Actions(driver);
			builder.clickAndHold(ele1).release(ele2).perform();
			reportStep("The element dropped successfully", "PASS");
		} catch (InvalidElementStateException e) {
			reportStep("The element not dropped successfully","FAIL");
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while dragging the elemnt", "FAIL");
		}
	}
	public String getText(WebElement ele) {	
		String bReturn = "";
		try {
			bReturn = ele.getText().trim();
			reportStep("The element text is : "+ele, "PASS");
		} catch (WebDriverException e) {
			reportStep("The element: "+ele+" could not be found.", "FAIL");
		}
		return bReturn;
	}
	public String getTitle() {		
		String bReturn = "";
		try {
			bReturn =  driver.getTitle();
		} catch (WebDriverException e) {
			reportStep("Unknown Exception Occured While fetching Title", "FAIL");
		} 
		return bReturn;
	}
	public String getAttribute(WebElement ele, String attribute) {		
		String bReturn = "";
		try {
			bReturn=  ele.getAttribute(attribute);
		} catch (WebDriverException e) {
			//edited by gayatri
			System.out.println("get attribute method");
			//second edit
			System.out.println("Print");
			reportStep("The element: "+ele+" could not be found.", "FAIL");
		} 
		return bReturn;
	}
	public void selectDropDownUsingVisibleText(WebElement ele, String value) {
		try {
			new Select(ele).selectByVisibleText(value);
			reportStep("The dropdown is selected with text "+value,"PASS");
		} catch (WebDriverException e) {
			reportStep("The element: "+ele+" could not be found.", "FAIL");
		}
	}
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			new Select(ele).selectByIndex(index);
			reportStep("The dropdown is selected with index "+index,"PASS");
		} catch (WebDriverException e) {
			reportStep("The element: "+ele+" could not be found.", "FAIL");
		} 

	}
	public boolean verifyExactTitle(String title) {
		boolean bReturn =false;
		try {
			if(getTitle().equals(title)) {
				reportStep("The title of the page matches with the value :"+title,"PASS");
				bReturn= true;
			}else {
				reportStep("The title of the page:"+driver.getTitle()+" did not match with the value :"+title, "FAIL");
			}
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while verifying the title", "FAIL");
		} 
		return bReturn;
	}
	public void verifyExactText(WebElement ele, String expectedText) {
		try {
			if(ele.getText().equals(expectedText)) { 
				reportStep("The text: "+ele.getText()+" matches with the value : "+expectedText,"PASS");
			}else {
				reportStep("The text: "+ele.getText()+" doesn't matches the actual "+expectedText,"FAIL");
			}
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while verifying the Text", "FAIL");
		} 
	}
	public String verifyTwoStringValues(String exactText, String expectedText) {
		try {
			if(exactText.equals(expectedText)) {
				reportStep("The text: "+expectedText+" matches with the value : "+expectedText,"PASS");
			}else {
				reportStep("The text "+exactText+" doesn't matches the actual "+expectedText,"FAIL");
			} 
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while verifying the Text", "FAIL");
		} 
		return expectedText;
	}
	public String verifyTwoStringPartialValues(String exactText, String expectedText) {
		try {
			if(exactText.contains(expectedText)) {
				reportStep("The text: "+expectedText+" matches with the value : "+expectedText,"PASS");
			}else {
				reportStep("The text "+exactText+" doesn't matches the actual "+expectedText,"FAIL");
			} 
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while verifying the Text", "FAIL");
		} 
		return expectedText;
	}

	public void verifyPartialText(WebElement ele, String expectedText) {
		try {
			String text = getText(ele); 
			if(text.contains(expectedText)) {
				reportStep("The expected text "+expectedText+" contains the actual "+text,"PASS");
			}else {
				reportStep("The expected text "+expectedText+" doesn't contain the actual "+text,"FAIL");
			}
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while verifying the Text", "FAIL");
		} 
	}

	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		try {
			if(getAttribute(ele, attribute).equals(value)) {
				reportStep("The expected attribute :"+attribute+" value matches the actual "+value,"PASS");
			}else {
				reportStep("The expected attribute :"+attribute+" value does not matches the actual "+value,"FAIL");
			}
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while verifying the Attribute Text", "FAIL");
		} 

	}

	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		try {
			if(getAttribute(ele, attribute).contains(value)) {
				reportStep("The expected attribute :"+attribute+" value contains the actual "+value,"PASS");
			}else {
				reportStep("The expected attribute :"+attribute+" value does not contains the actual "+value,"FAIL");
			}
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while verifying the Attribute Text", "FAIL");
		}
	}

	public void verifySelected(WebElement ele) {
		try {
			if(ele.isSelected()) {
				reportStep("The element "+ele+" is selected","PASS");
			} else {
				reportStep("The element "+ele+" is not selected","FAIL");
			}
		} catch (WebDriverException e) {
			reportStep("WebDriverException : "+e.getMessage(), "FAIL");
		}
	}

	public boolean verifySelectedBoolean(WebElement ele) {

		boolean selected = false;

		try {
			if(ele.isSelected()) {
				reportStep("The element "+ele+" is selected","PASS");
				selected = true;
			} else {
				reportStep("The element "+ele+" is not selected","FAIL");
				selected = false;
			}
		} catch (WebDriverException e) {
			reportStep("WebDriverException : "+e.getMessage(), "FAIL");
		}
		return selected;
	}

	public void verifyDisplayed(WebElement ele) {
		try {
			if(ele.isDisplayed()) {
				reportStep("The element "+ele+" is visible","PASS");
			} else {
				reportStep("The element "+ele+" is not visible","FAIL");
			}
		} catch (WebDriverException e) {
			reportStep("WebDriverException : "+e.getMessage(), "FAIL");
		} 
	}

	public static Set<String> allWindowHandles;
	public void switchToWindow(int index) {
		try {
			allWindowHandles = driver.getWindowHandles();
			List<String> allHandles = new ArrayList<>();
			System.out.println("Opened window count : "+allWindowHandles.size()); 
			allHandles.addAll(allWindowHandles);
			driver.switchTo().window(allHandles.get(index));
		} catch (NoSuchWindowException e) {
			reportStep("The driver could not move to the given window by index "+index,"FAIL");
		} catch (IndexOutOfBoundsException e) {
			reportStep("IndexOutOfBoundsException occured : "+index,"FAIL");
		} catch (WebDriverException e) {
			reportStep("WebDriverException : "+e.getMessage(), "FAIL");
		}
	}

	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
			reportStep("switch In to the Frame "+ele,"PASS");
		} catch (NoSuchFrameException e) {
			reportStep("WebDriverException : "+e.getMessage(), "FAIL");
		} catch (WebDriverException e) {
			reportStep("WebDriverException : "+e.getMessage(), "FAIL");
		} 
	}
	public void switchToFrame(int index) {
		try {
			driver.switchTo().frame(index);
			reportStep("switch In to the Frame passed","PASS");
		} catch (NoSuchFrameException e) {
			reportStep("WebDriverException : "+e.getMessage(), "FAIL");
		} catch (WebDriverException e) {
			reportStep("WebDriverException : "+e.getMessage(), "FAIL");
		} 
	}
	public void switchToFrame(String ele) {
		try {
			driver.switchTo().frame(ele);
			reportStep("switch In to the Frame "+ele,"PASS");
		} catch (NoSuchFrameException e) {
			reportStep("WebDriverException : "+e.getMessage(), "FAIL");
		} catch (WebDriverException e) {
			reportStep("WebDriverException : "+e.getMessage(), "FAIL");
		} 
	}
	public void defaultContent() {
		try {
			driver.switchTo().defaultContent();
			reportStep("comeout to the Frame ","PASS");
		} catch (NoSuchFrameException e) {
			reportStep("WebDriverException : "+e.getMessage(), "FAIL");
		} catch (WebDriverException e) {
			reportStep("WebDriverException : "+e.getMessage(), "FAIL");
		} 
	}

	public void acceptAlert() {
		String text = "";		
		try {
			Alert alert = driver.switchTo().alert();
			text = alert.getText();
			alert.accept();
			reportStep("The alert "+text+" is accepted.","PASS");
		} catch (NoAlertPresentException e) {
			reportStep("There is no alert present.","FAIL");
		} catch (WebDriverException e) {
			reportStep("WebDriverException : "+e.getMessage(), "FAIL");
		}  
	}

	public void dismissAlert() {
		String text = "";		
		try {
			Alert alert = driver.switchTo().alert();
			text = alert.getText();
			alert.dismiss();
			reportStep("The alert "+text+" is dismissed.","PASS");
		} catch (NoAlertPresentException e) {
			reportStep("There is no alert present.","FAIL");
		} catch (WebDriverException e) {
			reportStep("WebDriverException : "+e.getMessage(), "FAIL");
		} 

	}

	public String getAlertText() {
		String text = "";		
		try {
			Alert alert = driver.switchTo().alert();
			text = alert.getText();
		} catch (NoAlertPresentException e) {
			reportStep("There is no alert present.","FAIL");
		} catch (WebDriverException e) {
			reportStep("WebDriverException : "+e.getMessage(), "FAIL");
		} 
		return text;
	}

	public void scrollDown(String value) throws IOException, InterruptedException {
		try {
			Thread.sleep(3000);
			WebElement reg = locateElement("xpath", value);
			int y = reg.getLocation().getY();
			System.out.println("Location of y :"+ y); 
			Actions builder = new Actions(driver);
			builder.sendKeys(Keys.PAGE_DOWN).build().perform();
			/*((JavascriptExecutor) driver).executeScript("scroll(0,"+y+");");*/ 
			Thread.sleep(3000);
			reportStep("element scrolled", "PASS"); 
		} catch (WebDriverException e) {
			reportStep("WebDriverException : "+e.getMessage(), "FAIL");
		}
	} 
	public void scrolldown() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}
	public void scrolltoview(String value) {
		try {
			JavascriptExecutor je = (JavascriptExecutor) driver;
			// Identify the WebElement which will appear after scrolling down
			WebElement element = locateElement("xpath", value);
			// now execute query which actually will scroll until that element is
			// not appeared on page.
			je.executeScript("arguments[0].scrollIntoView(true);", element);
			reportStep("Element scrolled successfully", "PASS");
		} catch (Exception e) {
			reportStep("Element not scroll down", "FAIL");
		}
	}
	public void scrolltoviewAllElements(String value) {
		try {
			JavascriptExecutor je = (JavascriptExecutor) driver;
			// Identify the WebElement which will appear after scrolling down
			List<WebElement> element = locateElements("xpath", value);
			// now execute query which actually will scroll until that element is
			// not appeared on page.
			je.executeScript("arguments[0].scrollIntoView(true);", element);
			reportStep("Element scrolled", "PASS");
		} catch (Exception e) {
			reportStep("Element not scroll down", "FAIL");
		}
	}
	public void scroll() {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0, 650);");
	}
	public void threadSleep() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void waitUntilInvisibilityOfWebElement(String ele) throws InterruptedException{
		try {
			Thread.sleep(500);
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.invisibilityOf(driver.findElementByXPath(prop.getProperty(ele))));
			reportStep("Element invisible successfully", "PASS");
		} catch (WebDriverException e) { 
			reportStep("Thread time out", "FAIL");  
		} 
	}
	public void waitUntilVisibilityOfWebElement(String ele) {
		try {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.visibilityOf(driver.findElementByXPath(prop.getProperty(ele))));
			reportStep("Element visibled successfully", "PASS"); 
		} catch (WebDriverException e) { 
			reportStep("Thread time out", "FAIL");  
		} 
	}

	public void closeActiveBrowser() {
		try {
			driver.close();
			reportStep("The browser is closed","PASS", false);
		} catch (Exception e) {
			reportStep("The browser could not be closed","FAIL", false);
		}
	}

	public void closeAllBrowsers() {
		try {
			driver.quit();
			reportStep("The opened browsers are closed","PASS", false);
		} catch (Exception e) {
			reportStep("Unexpected error occured in Browser","FAIL", false);
		}
	}

	public void selectDropDownUsingValue(WebElement ele, String value) {
		try {
			new Select(ele).selectByValue(value);
			reportStep("The dropdown is selected with text "+value,"PASS");
		} catch (WebDriverException e) {
			reportStep("The element: "+ele+" could not be found.", "FAIL");
		}
	}
	long number;
	@Override
	public long takeSnap(){
		long number = (long) Math.floor(Math.random() * 900000000L) + 10000000L; 
		try {
			FileUtils.copyFile(driver.getScreenshotAs(OutputType.FILE) , new File("src\\main\\java\\reports\\images\\"+number+".jpg"));
		} catch (WebDriverException e) {
			System.out.println("The browser has been closed."); 
		} catch (IOException e) {
			System.out.println("The snapshot could not be taken");
		}
		return number;
	}
	public boolean verifyPartialTitle(String title) {
		boolean bReturn =false;
		try {
			if(getTitle().contains(title)) {
				reportStep("The title of the page matches with the value :"+title,"PASS");
				bReturn= true;
			}else {
				reportStep("The title of the page:"+driver.getTitle()+" did not match with the value :"+title, "FAIL");
			}
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while verifying the title", "FAIL");
		} 
		return bReturn;		
	}
	public boolean verifyEnabled(WebElement ele) {
		boolean enabled = false;
		try {
			if(ele.isEnabled()) {				
				enabled = true;
			} else {				
				enabled = false;
			}
		} catch (WebDriverException e) {
			reportStep("WebDriverException : "+e.getMessage(), "FAIL");
		}
		return enabled; 
	}
	public void keysEnter() {
		driver.getKeyboard().sendKeys(Keys.DOWN); 
		driver.getKeyboard().sendKeys(Keys.ENTER); 
	}
	/*****************************************************************************************************/


	@SuppressWarnings("deprecation")
	public String readData(int coloum, int rows, String sheetname, String fileName) throws IOException {
		//String fileName = null;
		String data = null;
		Workbook wb = null;
		fileName = "./src/main/java/data/"+fileName+".xlsx";
		FileInputStream fileInputStream = new FileInputStream(fileName);
		String fileExtensionName = fileName.substring(fileName.lastIndexOf("."));
		if (fileExtensionName.equals(".xlsx")) {
			wb = new XSSFWorkbook(fileInputStream);
			XSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
		} else if (fileExtensionName.equals(".xls")) {
			wb = new HSSFWorkbook(fileInputStream);
			HSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
		}
		Sheet sheet = wb.getSheet(sheetname);
		Row row = sheet.getRow(rows);
		Cell cell = row.getCell(coloum);
		cell.setCellType(Cell.CELL_TYPE_STRING);

		if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
			System.out.println("string: " + cell.getStringCellValue());
			data = cell.getStringCellValue();
		}
		if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
			System.out.println("numeric: " + cell.getNumericCellValue());
		}
		System.out.println("any: " + cell.toString());
		data = cell.toString();
		return data;
	}

	@SuppressWarnings({ "null", "deprecation" })
	public void writeData(int coloum, int rows, String text, String sheetname, String fileName, String color)
			throws EncryptedDocumentException, InvalidFormatException, IOException {
		//String fileName =null;
		Workbook wb1 = null;
		Sheet sheet;
		Cell cell;
		Row row;
		CellStyle style = null;
		String rootPath = System.getProperty("user.dir");
		fileName = rootPath+"\\src\\main\\java\\data\\"+fileName+".xlsx";
		FileInputStream fileInputStream = new FileInputStream(fileName);
		String fileExtensionName = fileName.substring(fileName.lastIndexOf("."));
		if (fileExtensionName.equals(".xlsx")) {
			wb1 = new XSSFWorkbook(fileInputStream);
			XSSFFormulaEvaluator.evaluateAllFormulaCells(wb1);
			style = wb1.createCellStyle();

			Font font = wb1.createFont();
			if (color.equalsIgnoreCase("Green")) {
				font.setColor(IndexedColors.GREEN.getIndex());
			} else if (color.equalsIgnoreCase("Red")) {
				font.setColor(IndexedColors.RED.getIndex());
			} else {
				font.setColor(IndexedColors.BLACK.getIndex());
			}
			style.setFont(font);
		}	else if (fileExtensionName.equals(".xls")) {
			wb1 = new HSSFWorkbook(fileInputStream);
			HSSFFormulaEvaluator.evaluateAllFormulaCells(wb1);
			style = wb1.createCellStyle();
			Font font = wb1.createFont();

			if (color.equalsIgnoreCase("Green")) {
				font.setColor(HSSFColor.GREEN.index);
			} else if (color.equalsIgnoreCase("Red")) {
				font.setColor(HSSFColor.RED.index);
			} else {
				font.setColor(HSSFColor.BLACK.index);
			}
			style.setFont(font);
		}
		if (wb1 == null) {
			sheet = wb1.createSheet();
		}

		sheet = wb1.getSheet(sheetname);
		row = sheet.getRow(rows);
		if (row == null) {
			row = sheet.createRow(rows);
		}
		cell = row.getCell(coloum);
		if (cell == null)
			cell = row.createCell(coloum);
		cell.setCellType(Cell.CELL_TYPE_STRING);
		cell.setCellValue(text);
		cell.setCellStyle(style);
		FileOutputStream fileOut = new FileOutputStream(fileName);
		wb1.write(fileOut);
		fileOut.close();
	}

	@SuppressWarnings("null")
	public void writeDataToLiveire(int coloum, int rows, String text, String sheetname, String fileName, String color)
			throws EncryptedDocumentException, InvalidFormatException, IOException {
		//String fileName =null;
		Workbook wb1 = null;
		Sheet sheet;
		Cell cell;    
		Row row;
		CellStyle style = null;
		fileName = "./src/main/java/data/csvfiles/Livewire Files/"+fileName+".xlsx";
		FileInputStream fileInputStream = new FileInputStream(fileName);
		String fileExtensionName = fileName.substring(fileName.lastIndexOf("."));
		if (fileExtensionName.equals(".xlsx")) {
			wb1 = new XSSFWorkbook(fileInputStream);
			XSSFFormulaEvaluator.evaluateAllFormulaCells(wb1);
			style = wb1.createCellStyle();
			Font font = wb1.createFont();
			if (color.equalsIgnoreCase("Green")) {
				font.setColor(IndexedColors.GREEN.getIndex());
			} else if (color.equalsIgnoreCase("Red")) {
				font.setColor(IndexedColors.RED.getIndex());
			} else {
				font.setColor(IndexedColors.BLACK.getIndex());
			}
			style.setFont(font);
		} else if (fileExtensionName.equals(".xls")) {
			wb1 = new HSSFWorkbook(fileInputStream);
			HSSFFormulaEvaluator.evaluateAllFormulaCells(wb1);
			style = wb1.createCellStyle();
			Font font = wb1.createFont();
			style.setFont(font);
		}
		if (wb1 == null) {
			sheet = wb1.createSheet();
		}

		sheet = wb1.getSheet(sheetname);
		row = sheet.getRow(rows);
		if (row == null) {
			row = sheet.createRow(rows);
		}
		cell = row.getCell(coloum);
		if (cell == null)
			cell = row.createCell(coloum);
		cell.setCellValue(text);
		cell.setCellStyle(style);
		FileOutputStream fileOut = new FileOutputStream(fileName); 
		wb1.write(fileOut);
		wb1.close();
		fileOut.close();
	}

	public int getRowNo(String sheetname, String fileName) throws IOException {
		//String fileName =null;
		Workbook wb = null;
			fileName = "./src/main/java/data/"+fileName+".xlsx";
		// System.err.println("File path is : "+fileName);
		FileInputStream fileInputStream = new FileInputStream(fileName);
		// fileInputStream.reset();
		String fileExtensionName = fileName.substring(fileName.lastIndexOf("."));
		// System.err.println("File Extension : "+fileExtensionName);
		if (fileExtensionName.equals(".xlsx")) {
			wb = new XSSFWorkbook(fileInputStream);			
			//XSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
		} else if (fileExtensionName.equals(".xls")) {
			wb = new HSSFWorkbook(fileInputStream);
			//HSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
		}
		Sheet sheet = wb.getSheet(sheetname);
		int lastRowNum = sheet.getLastRowNum();

		return lastRowNum;
	}

	public static void uploadfile(String Pathtxt) throws AWTException, InterruptedException {
		Thread.sleep(5000);
		// Copy your file's absolute path to the clipboard
		StringSelection ss = new StringSelection(Pathtxt);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
		// Paste the file's absolute path into the File name field of the File
		// Upload dialog box
		// native key strokes for CTRL, V and ENTER keys
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.delay(500);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}

	public void dateSelection(String data, String sDate, String eDate) throws Exception{		
		//click(locateElement("xpath", data));	
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Date givenSdate = dateFormat.parse(sDate);
		//get start Date month year seperately in dd mmm yyyy formats seperately
		String setSdate = new SimpleDateFormat("dd").format(givenSdate);
		String setSmonth = new SimpleDateFormat("MMM").format(givenSdate);		
		String setSyear = new SimpleDateFormat("yyyy").format(givenSdate);
		int sYear = Integer.parseInt(setSyear);		
		Date givenEdate = dateFormat.parse(eDate);
		//get end Date month year seperately in dd mmm yyyy formats seperately
		String setEdate = new SimpleDateFormat("dd").format(givenEdate); 
		String setEmonth = new SimpleDateFormat("MMM").format(givenEdate);		
		String setEyear = new SimpleDateFormat("yyyy").format(givenEdate);
		int eYear = Integer.parseInt(setEyear);	
		String displayedMY1 = getText(locateElement("xpath", "//th[@class='month']"));
		String [] disp1 = displayedMY1.split(" "); int dispYear1 =  Integer.parseInt(disp1[1]);
		// Select Start Date
		while((sYear<dispYear1)||(!displayedMY1.equalsIgnoreCase(setSmonth+" "+setSyear)))
		{
			locateElement("xpath", "//th[@class='prev available']/i").click();
			displayedMY1 = getText(locateElement("xpath", "//th[@class='month']"));								
			disp1 = displayedMY1.split(" ");
			dispYear1 =  Integer.parseInt(disp1[1]);			
		}
		List<WebElement> DatesList1 = driver.findElements(By.xpath("//th[text()='"+setSmonth+" "+setSyear+"']/../../../tbody/tr/td"));

		for(WebElement Date: DatesList1)
		{
			String dateElement = Date.getText();
			String dateElementClass = Date.getAttribute("class");
			if(dateElement.length()==1){dateElement="0"+dateElement;}
			if((dateElement.equalsIgnoreCase(setSdate))&&(dateElementClass.equalsIgnoreCase("available")))
			{				
				Date.click();
				Thread.sleep(5000);				
				break;
			}
		}
		// Select End Date
		String displayedMY2 = getText(locateElement("xpath", "(//th[@class='month'])[2]"));
		String [] disp2 = displayedMY2.split(" "); int dispYear2 =  Integer.parseInt(disp2[1]);

		while(((dispYear1<eYear)&&(dispYear2<eYear))||((!displayedMY1.equalsIgnoreCase(setEmonth+" "+setEyear))&&
				(!displayedMY2.equalsIgnoreCase(setEmonth+" "+setEyear))))
		{
			locateElement("xpath", "//th[@class='next available']/i").click();
			displayedMY1 = getText(locateElement("xpath", "//th[@class='month']"));								
			disp1 = displayedMY1.split(" ");
			dispYear1 =  Integer.parseInt(disp1[1]);
			displayedMY2 = getText(locateElement("xpath", "(//th[@class='month'])[2]"));								
			disp2 = displayedMY2.split(" ");
			dispYear2 =  Integer.parseInt(disp2[1]);			
		}

		List<WebElement> DatesList2 = driver.findElements(By.xpath("//th[text()='"+setEmonth+" "+setEyear+"']/../../../tbody/tr/td"));

		for(WebElement Date: DatesList2)
		{
			String dateElement = Date.getText();
			String dateElementClass = Date.getAttribute("class");
			if(dateElement.length()==1){dateElement="0"+dateElement;}
			if((dateElement.equalsIgnoreCase(setEdate))&&(dateElementClass.equalsIgnoreCase("available")))
			{
				Date.click();
				Thread.sleep(5000);				
				break;
			}
		}
	}

	

}
