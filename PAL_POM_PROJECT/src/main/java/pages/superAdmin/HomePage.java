package pages.superAdmin;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class HomePage extends  PreAndPost{

	/*
	 * public HomePage() { PageFactory.initElements(driver, this); }
	 */
	
	public HomePage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test   = test;
	}
	
	public HomePage verifySuperAdminPageTitle(String title) {
		verifyExactTitle(title);
		return this;
		
	}
	
	public HomePage clickUserIcon() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		click(locateElement("xpath", "HomePage.xpath.userImg"));
		return this;
	}
	
	public LoginPage clickSignout() {
		click(locateElement("link", "HomePage.link.signOut"));
		return new LoginPage(driver, test); 
	}
	
}


