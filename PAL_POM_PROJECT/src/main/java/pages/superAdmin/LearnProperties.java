package pages.superAdmin;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class LearnProperties {

	
	public static void main(String[] args) throws IOException {
		FileInputStream file = null;
		try {
			file = new FileInputStream("./src/main/java/propertiesFile/sample.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		Properties prop = new Properties();
		prop.load(file);
		
		String email = prop.getProperty("LoginPage.xpath.signIn");
		System.out.println(email); 
		
		System.out.println(System.getProperty("user.dir"));
	}
}
