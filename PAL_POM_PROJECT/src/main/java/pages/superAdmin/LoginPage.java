package pages.superAdmin;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class LoginPage extends PreAndPost{

	/*
	 * public LoginPage() { PageFactory.initElements(driver, this); }
	 */
	
	public LoginPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test   = test;
	}

	String var = "";

	@FindBy(how = How.ID, using="email") 
	WebElement eleUsername;
	public void enterUsername(String data) {
		type(eleUsername, data); 
	}

	@FindBy(how = How.ID, using ="password")  WebElement elePassword; 
	public void enterPassword(String data) {
		type(elePassword, data);
		locateElement("id", "password");
	}

	@FindBy(how = How.XPATH, using ="//button[text()='Sign In']")  WebElement eleSubmit;
	public void clickSubmit() {
		click(eleSubmit);
	}


	public LoginPage startSuperAdminLogin(String domain) {
		if(domain.equals("newdomain01")) {
			loadUrl("newdomain01.sa.url");
		} else if(domain.equals("newdomain02")) {
			loadUrl("newdomain02.sa.url"); 
		}
		return this;
	}
	
	public LoginPage startAdminLogin(String domain) {
		if(domain.equals("newdomain01")) {
			loadUrl("newdomain01.admin.url");
		} else if(domain.equals("newdomain02")) {
			loadUrl("newdomain02.admin.url"); 
		}
		return this;
	}
	public LoginPage enterUname(String data) {
		type(locateElement("id", "LoginPage.id.email"), data); 
		/*
		 * LoginPage lp = new LoginPage(); return lp;
		 * 
		 */
		return this;
	}
	
	public LoginPage enterPwd(String data) {
		type(locateElement("id", "LoginPage.id.psw"), data); 
		//return new LoginPage(); 
		return this;
	}
	
	public HomePage clickSubmitButton() {
		click(locateElement("xpath", "LoginPage.xpath.signIn")); 
		return new HomePage(driver, test);
		
	}
	
	





















}
