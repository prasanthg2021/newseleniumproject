package testcases.SuperAdmin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.superAdmin.LoginPage;

public class TC001_SuperAdminLogin extends PreAndPost{

	
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_SuperAdminLogin";
		testDescription = "Login into newdomain01 super-admin";
		nodes  = "Login";
		author = "Prasanth";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "SuperAdminLogin";
	}
	
	
	@Test(dataProvider = "fetchData")
	public void superAdminLogin(String domain, String uname, String pwd, String title) {
		
		
		new LoginPage(driver, test)
		.startSuperAdminLogin(domain)
		.enterUname(uname)
		.enterPwd(pwd)
		.clickSubmitButton()
		.verifySuperAdminPageTitle(title)
		.clickUserIcon() 
		.clickSignout();
		
		
		
		
		
		
		/*
		 * LoginPage lp = new LoginPage();
		 * 
		 * lp.startSuperAdminLogin(domain); lp.enterUname(uname); lp.enterPwd(pwd);
		 * lp.clickSubmit();
		 * 
		 * HomePage hp = new HomePage(); hp.verifySuperAdminPageTitle(title);
		 */
		
	}
}


























